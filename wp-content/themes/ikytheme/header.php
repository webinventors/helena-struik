<!doctype html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">

		<?php wp_head(); ?>
	</head>

<body <?php body_class(); ?>>

<div id='page-wrapper'>

	<header id='main-header'>

		<?php get_template_part('header-templates/content', 'header'); ?>

	</header>
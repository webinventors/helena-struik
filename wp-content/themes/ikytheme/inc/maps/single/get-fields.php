<?php

/**
 * @author 		Wilco
 * @since 		16-5
 * @version 	3.0
 */

/*-------------------------------------------------------------------------------
	Get map location function
-------------------------------------------------------------------------------*/

if( ! function_exists( 'get_map_location' ) ) {

	/**
	 *
	 * @return string
	 */

	function get_map_location( $field, $location_field_name, $current_post ){

		// If field is not a subfield
		if( $field == 'normal' ){

			$get_field = 'get_field';
			$get_object = 'get_field_object';

		// If field is a subfield
		}elseif( $field == 'sub' ){

			$get_field = 'get_sub_field';
			$get_object = 'get_sub_field_object';
		}

		// Get location
		$location = $get_field( $location_field_name , $current_post );

		// Return Location
		return $location;
	}
}

/*-------------------------------------------------------------------------------
	Get marker image function
-------------------------------------------------------------------------------*/

if( ! function_exists( 'get_marker_image' ) ) {

	function get_marker_image( $field, $field_name, $current_post ){

		if($field = 'normal' ){

			$get_field = 'get_field';
			$get_object = 'get_field_object';

		}elseif( $field = 'sub'){

			$get_field = 'get_sub_field';
			$get_object = 'get_sub_field_object';
		}

		$object = $get_object( $field_name, $current_post );

		// Type = IMAGE
		if($object['type'] == 'image' ){

			// Return format = URL
			if($object['return_format'] == 'url' ){

				$marker_image = $get_field( $field_name, $current_post );

			// Return format = ARRAY
			}elseif( $object['return_format'] == 'array' ) {

				$marker_image = $get_field( $field_name, $current_post)['url'];

			// Return format = ID
			}elseif( $object['return_format'] == 'id' ) {

				$marker_image = wp_get_attachment_url( $get_field( $field_name, $current_post ) );

			// Non-valid return format
			}elseif( is_user_logged_in() && $object['return_format'] != 'array' && $object['return_format'] != 'url' && $object['return_format'] != 'id' ) {

				echo '"'.$field_name.'" '.__( 'must have return format of "URL" or "ARRAY"' );
				$marker_image = '';

			}else{

				$marker_image = '';
			}

		// Type != IMAGE && User = logged in
		}elseif( is_user_logged_in() ){

			echo '"'.$field_name.'" is not an image field!';
			$marker_image = '';

		// Type != IMAGE && User != not logged in
		}else{

			$marker_image = '';
		}

		// Marker image doesnt exist
		if( ! isset( $marker_image ) ) {

			$marker_image = '';
		}

		// Return Marker image
		return $marker_image;
	}// end function
}

/*-------------------------------------------------------------------------------
	Project module
-------------------------------------------------------------------------------*/

// Project module
if( get_field( 'maps_setting_use_project_module_name') == 0 ) {

	// If Location name is set
	if( get_field( 'maps_setting_location_projects_field_name' ) ) {

		// Get location field name
		$field_name = get_field( 'maps_setting_location_projects_field_name');

		// If post type is selected
		if( get_field( 'maps_setting_posttype_name' ) ) {

			$post_type = get_field( 'maps_setting_posttype_name');
		}else{

			$post_type = '';
		}

		$get_maps_projects = array(

			'post_type'	=> $post_type,
		);

		$the_maps_projects_query = new WP_Query( $get_maps_projects );

	// Location name is not set
	}else{

		$location = '';
	}

/*-------------------------------------------------------------------------------
	Normale module 
-------------------------------------------------------------------------------*/

}elseif( get_field( 'maps_setting_use_project_module_name' ) == 1 || empty( 'maps_setting_use_project_module_name' ) ){

	// If map on true ( = map intern )
	if( get_field( 'maps_setting_switch_map_name' ) == '1' ) {

		$location = get_field( 'maps_setting_location_name' );

	// If map on false ( = map extern )
	}else{

		// If ACF location field name is set;
		if( get_field( 'maps_setting_location_field_name' ) ) {

			$location_field_name = get_field( 'maps_setting_location_field_name' );

			// Get field
			if( get_field( $location_field_name , $current_post_id ) ) {

				$location = get_map_location( 'normal', $location_field_name, $current_post_id );

			// Get subfield
			}elseif( get_sub_field( $location_field_name , $current_post_id ) ) {

				$location = get_map_location( 'sub', $location_field_name, $current_post_id );
			}else{

				$location = '';
			}

		}else{

			$location = '';
		}
	} // End else map = extern

	// If marker image is on map
	if( get_field( 'maps_setting_switch_marker_name' ) == '1' ) {

		// If marker image on map exists
		if( get_field( 'maps_setting_marker_image_name' ) ) {

			// Create variable for marker image
			$marker_image = get_field( 'maps_setting_marker_image_name' );

		// If marker image on map doesn't exist
		}else{

			// Create empty variable for marker image.
			$marker_image = '';
		}

	// If marker image is on post
	}elseif( get_field( 'maps_setting_switch_marker_name' ) == 0 ) {

		// If acf field name for marker image field exists
		if( get_field( 'maps_setting_marker_field_name' ) ) { 

			// Create variable for marker image acf field name
			$field_name = get_field( 'maps_setting_marker_field_name' );

		}else{

			// Use default field name 
			$field_name = 'marker';
		}

		// Marker image is not a subfield
		if( get_field( $field_name, $current_post_id ) ) {

			// Create variable Marker image from function
			$marker_image = get_marker_image( 'normal', $field_name, $current_post_id);

		// Marker image is a subfield
		}elseif( get_sub_field( $field_name, $current_post_id ) ) {

			// Create variable Marker image from function = subfield
			$marker_image = get_marker_image( 'sub', $field_name, $current_post_id);

		// Not a normal field or a subfield
		}else{
			
			// Create empty variable for marker image.
			$marker_image = '';
		}
	} // End elseif marker image is on post
}


// Marker image doesnt exist
if( !isset($marker_image) ){

	$marker_image = '';
}


$zoom_level = get_field( 'maps_setting_zoom_field_name' );

if( empty($zoom_level) ) {

	$zoom_level = '15';
}

// Create data
$data = array(
	'maps' => array(
		'marker_image' 		=> (string) $marker_image,
		'zoom_level'		=> (int) $zoom_level,
	),
);
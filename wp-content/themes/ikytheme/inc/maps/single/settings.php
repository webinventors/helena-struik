<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if( ! function_exists( 'min_maps_single_local_field_group' ) ) {

	function min_maps_single_local_field_group() {

		// Side group
		acf_add_local_field_group( array(
	        'key'      => 'min_maps_single_group_side',
	        'title'    => __( 'Shortcode','minimal210' ),
			'location' => array(
				array(
					array(
						'param' 	=> 'post_type',
						'operator' 	=> '==',
						'value' 	=> 'min_maps_single',
					),
				),
			),
	        'menu_order'            => 0,
	        'position'              => 'side',
	        'style'                 => 'default',
	        'label_placement'       => 'top',
	        'instruction_placement' => 'label',
	        'hide_on_screen'        => '',
	        'active'				=> 1,
	        'description'			=> '',
	    ) );

		// Shortcode in side
	    acf_add_local_field( array(
	        'key'					=> 'field_side_shortcode',
	        'name'         			=> 'side_shortcode',
	        'parent'       			=> 'min_maps_single_group_side',
	        'type'         			=> 'message',
	        'message'				=> '',
	        'instructions'			=> __( 'Use this code to display your map','minimal210' ),
   		) );

	    // Maps settings group
		acf_add_local_field_group( array(
	        'key'      				=> 'min_maps_single_group_main',
	        'title'    				=> __( 'Map settings','minimal210' ),
			'location' 				=> array(
				array(
					array(
						'param' 	=> 'post_type',
						'operator' 	=> '==',
						'value' 	=> 'min_maps_single',
					),
				),
			),
	        'menu_order'            => 0,
	        'position'              => 'normal',
	        'style'                 => 'default',
	        'label_placement'       => 'top',
	        'instruction_placement' => 'label',
	        'hide_on_screen'        => '',
	        'active'				=> 1,
	        'description'			=> '',
	    ) );	

		// General tab
		acf_add_local_field( array(
	        'key'          			=> 'field_tab_general',
	        'label'        			=> __( 'General','minimal210' ),
	        'name'         			=> '',
	        'type'         			=> 'tab',
	        'endpoint'				=> 0,
	        'parent'       			=> 'min_maps_single_group_main',
   		) );

		// Location
		acf_add_local_field( array(
	        'key'					=> 'field_map_location',
	        'label'        			=> __( 'Location for the map','minimal210' ),
	        'name'         			=> 'map_location',
	        'type'         			=> 'true_false',
	        'parent'       			=> 'min_maps_single_group_main',
	        'default_value' 		=> false,
			'ui' 					=> 1,
			'ui_on_text' 			=> 'Post',
			'ui_off_text' 			=> 'Map',
			'wrapper' 				=> array(
					'width' 		=> '30',
					'class' 		=> '',
					'id' 			=> '',
				),
		) );

		// ACF map
		acf_add_local_field( array(
	        'key'					=> 'field_acf_map',
	        'label'        			=> __( 'Map', 'minimal210' ),
	        'name'         			=> 'acf_map',
	        'type'         			=> 'google_map',
	        'parent'       			=> 'min_maps_single_group_main',
	        'instructions' 			=> '',
	        'wrapper' 				=> array(
					'width' 		=> '70',
					'class' 		=> '',
					'id' 			=> '',
				),
	        'conditional_logic' 	=> array(
				array(
					array(
						'field' 	=> 'field_map_location',
						'operator' 	=> '!=',
						'value' 	=> '1',
					),
				),
			),
		) );

		// ACF location name
		acf_add_local_field( array(
	        'key'					=> 'field_acf_location_name',
	        'label'        			=> __( 'ACF Field name from the google maps field', 'minimal210' ),
	        'name'         			=> 'acf_location_name',
	        'type'         			=> 'text',
	        'parent'       			=> 'min_maps_single_group_main',
	        'placeholder'			=> __( 'location', 'minimal210' ),
	        'default_value'			=> 'location',
	        'instructions' 			=> __( 'Defaults to &quot;location&quot;.', 'minimal210' ),
	        'wrapper' 				=> array(
					'width' 		=> '70',
					'class' 		=> '',
					'id' 			=> '',
				),
       		'conditional_logic' 	=> array(
				array(
					array(
						'field' 	=> 'field_map_location',
						'operator' 	=> '==',
						'value' 	=> '1',
					),
				),
			),
		) );

		acf_add_local_field( array(
	        'key'					=> 'field_marker_location',
	        'label'        			=> __( 'Location for the marker image','minimal210' ),
	        'name'         			=> 'marker_location',
	        'type'         			=> 'true_false',
	        'parent'       			=> 'min_maps_single_group_main',
	        'default_value' 		=> false,
			'ui' 					=> 1,
			'ui_on_text' 			=> 'Post',
			'ui_off_text' 			=> 'Map',
			'wrapper' 				=> array(
					'width' 		=> '30',
					'class' 		=> '',
					'id' 			=> '',
				),
		) );

		acf_add_local_field( array(
	        'key'					=> 'field_marker_image',
	        'label'        			=> __( 'Image to use as marker image','minimal210' ),
	        'name'         			=> 'marker_image',
	        'type'         			=> 'image',
	        'parent'       			=> 'min_maps_single_group_main',
	        'instructions' 			=> __( 'Keep empty to use the default marker','minimal210' ),
	        'return_format'			=> 'url',
	        'wrapper' 				=> array(
					'width' 		=> '70',
					'class' 		=> '',
					'id' 			=> '',
				),
	        'conditional_logic' 	=> array(
				array(
					array(
						'field' 	=> 'field_marker_location',
						'operator' 	=> '!=',
						'value' 	=> '1',
					),
				),
			),
		) );

		acf_add_local_field( array(
	        'key'					=> 'field_marker_location_acf_name',
	        'label'        			=> __( 'ACF Field name from the marker field','minimal210' ),
	        'name'         			=> 'marker_location_acf_name',
	        'type'         			=> 'text',
	        'parent'       			=> 'min_maps_single_group_main',
	        'placeholder'			=> __( 'marker','minimal210' ),
	        'default_value'			=> 'marker',
	        'instructions' 			=> __( 'Defaults to &quot;marker&quot;.','minimal210' ),
	        'wrapper' 				=> array(
					'width' 		=> '70',
					'class' 		=> '',
					'id' 			=> '',
				),
       		'conditional_logic' 	=> array(
				array(
					array(
						'field' 	=> 'field_marker_location',
						'operator' 	=> '==',
						'value' 	=> '1',
					),
				),
			),
		) );

		acf_add_local_field( array(
	        'key'          			=> 'field_tab_related',
	        'label'        			=> __( 'Map related','minimal210' ),
	        'name'         			=> '',
	        'type'         			=> 'tab',
	        'endpoint'				=> 1,
	        'parent'       			=> 'min_maps_single_group_main',
   		) );

		acf_add_local_field( array(
	        'key'					=> 'field_zoom_level',
	        'label'        			=> __( 'Zoom level','minimal210' ),
	        'name'         			=> 'zoom_level',
	        'type'         			=> 'number',
	        'parent'       			=> 'min_maps_single_group_main',
	        'instructions' 			=> '',
	        'default_value'			=> 15,
	        'min'					=> 1,
	        'step'					=> 1,
	        'max'					=> 20,
	        'wrapper' 				=> array(
					'width' 		=> '20',
					'class' 		=> '',
					'id' 			=> '',
				),
		) );

		acf_add_local_field( array(
	        'key'					=> 'field_zoom_level_instructions',
	        'label'        			=> __( 'Zoom level instructions','minimal210' ),
	        'name'         			=> '',
	        'type'         			=> 'message',
	        'parent'       			=> 'min_maps_single_group_main',
	        'instructions' 			=> __( 'The following list shows the approximate level of detail you can expect to see at each zoom level: <br> <br>
									1: World <br>
									5: Landmass/continent <br>
									10: City <br>
									15: Streets <br>
									20: Buildings','minimal210' ),
	        'new_lines' 			=> '',
	        'wrapper' 				=> array(
					'width' 		=> '80',
					'class' 		=> '',
					'id' 			=> '',
				),
		) );

	}
	add_action( 'init', 'min_maps_single_local_field_group' );
}

function alter_maps_side_message( $field ) {

	global $post;

	$field['message'] = '<input onclick="this.focus();this.select()" value="[map '.get_the_id().']">';

	return $field;
}
add_filter('acf/prepare_field/name=field_side_shortcode', 'alter_maps_side_message');


?>
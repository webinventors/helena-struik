// IE CHECK
function msieversion() {

	var ua = window.navigator.userAgent;
	var msie = ua.indexOf("MSIE ");

	if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./) ) {

		return true;
	}
	else {
		return false;
	}

	return false;
}

jQuery(window).load( function(){

	// Call flexslider with args from slider.php
	jQuery('.flexslider').flexslider({

		animation 				: arg.slider.animation,
		easing					: arg.slider.easing,
		directionNav 			: arg.slider.directionNav,
		prevText				: arg.slider.prevText,
		nextText 				: arg.slider.nextText,
		controlNav 				: arg.slider.controlNav,
		animationLoop 			: arg.slider.animationLoop,
		direction 				: arg.slider.direction,
		randomize 				: arg.slider.randomize,
		slideshowSpeed 			: arg.slider.slideshowSpeed,
		animationSpeed 			: arg.slider.animationSpeed,
		pauseOnHover 			: arg.slider.pauseOnHover,
		reverse					: arg.slider.reverse,
		animationDuration		: 0,

		// Advanced settings
		useCSS 					: arg.slider.useCSS,
		keyboard 				: arg.slider.keyboard,
		touch 					: arg.slider.touch,
		controlsContainer 		: arg.slider.controlsContainer,
		customDirectionNav 		: arg.slider.customDirectionNav,
		manualControls			: arg.slider.customManualcontrols,

		pauseOnAction			: true,

		after: function(slider) {
			/* auto-restart player if paused after action */
			if (!slider.playing) {
				slider.play();
			}
			jQuery(window).trigger('resize');
		}
	});

	// If setting "IE FADE FIX" is enabled - If animation type is "fade" - If is IE
	if(arg.slider.ieFade && arg.slider.animation == 'fade' && msieversion() ){

		// Get speed from animationSpeed
		var speed = '0.'+arg.slider.animationSpeed+'s';

		// Apply css
		jQuery('ul.slides li').css({'transition-property':'opacity','transition-duration':speed, 'transition-timing-function':'ease','transition-delay':'0s'});
	}
});
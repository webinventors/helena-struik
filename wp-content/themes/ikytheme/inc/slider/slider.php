<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/*-------------------------------------------------------------------------------
	Enqueue scripts and styles
-------------------------------------------------------------------------------*/

if( ! function_exists( 'minimal_slider_scripts' ) ) {

	function minimal_slider_scripts() {

		// Create path
		$slider_path = get_template_directory_uri(). '/inc/slider';

		// Frontend styles
		wp_register_style( 'minimal210-slider-style', $slider_path. '/css/flexslider.css' );

		// Custom frontend styles
		wp_register_style( 'minimal210-theme-slider-style', $slider_path. '/css/theme-slider.css' );

		// Flexslider library
		wp_register_script( 'minimal210-slider-flexslider', 'https://cdnjs.cloudflare.com/ajax/libs/flexslider/2.6.3/jquery.flexslider.min.js', array('jquery') );

		// Register local script
		wp_register_script( 'minimal210-slider-local', $slider_path. '/js/local.js', array( 'jquery' ) );
	}
	add_action( 'wp_enqueue_scripts', 'minimal_slider_scripts' );
}

/*-------------------------------------------------------------------------------
	Enqueue admin scripts and styles
-------------------------------------------------------------------------------*/

if( ! function_exists('minimal_slider_adminscripts') ) {

	function minimal_slider_adminscripts() {

		// Create path
		$slider_admin_path = get_template_directory_uri(). '/inc/slider/admin';

		// This page only
		$page = 'acf-options-settings';

		// bail early if not correct page
		if( empty($_GET['page']) || $_GET['page'] !== $page ) {
			return;
		}

		// Not needed
		wp_dequeue_script( 'jquery-ui-datepicker' );

		// Settings page css
		wp_enqueue_style( 'minimal-slider-admin-style', $slider_admin_path. '/css/admin.css' );
	}
	add_action( 'admin_enqueue_scripts', 'minimal_slider_adminscripts' );
}

/*-------------------------------------------------------------------------------
	Slider post type
-------------------------------------------------------------------------------*/

if ( ! function_exists('minimal_slider') ) {

	function minimal_slider() {

		$labels = array(
			'name'                  => _x( 'Slides', 'minimal210' ),
			'singular_name'         => _x( 'Slide',  'minimal210' ),
			'menu_name'             => __( 'Slider', 'minimal210' ),
			'name_admin_bar'        => __( 'Slider', 'minimal210' ),
			'archives'              => __( 'Slider Archives', 'minimal210' ),
			'attributes'            => __( 'Slider Attributes', 'minimal210' ),
			'parent_item_colon'     => __( 'Parent slider:', 'minimal210' ),
			'all_items'             => __( 'All slides', 'minimal210' ),
			'add_new_item'          => __( 'Add New slide', 'minimal210' ),
			'add_new'               => __( 'Add slide', 'minimal210' ),
			'new_item'              => __( 'New slide', 'minimal210' ),
			'edit_item'             => __( 'Edit slide', 'minimal210' ),
			'update_item'           => __( 'Update slide', 'minimal210' ),
			'view_item'             => __( 'View slide', 'minimal210' ),
			'view_items'            => __( 'View slides', 'minimal210' ),
			'search_items'          => __( 'Search slides', 'minimal210' ),
			'not_found'             => __( 'Not found', 'minimal210' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'minimal210' ),
			'featured_image'        => __( 'Slide Image', 'minimal210' ),
			'set_featured_image'    => __( 'Set slide image', 'minimal210' ),
			'remove_featured_image' => __( 'Remove slide image', 'minimal210' ),
			'use_featured_image'    => __( 'Use as slide image', 'minimal210' ),
			'insert_into_item'      => __( 'Insert into slide', 'minimal210' ),
			'uploaded_to_this_item' => __( 'Uploaded to this slide', 'minimal210' ),
			'items_list'            => __( 'Slides list', 'minimal210' ),
			'items_list_navigation' => __( 'Slides list navigation', 'minimal210' ),
			'filter_items_list'     => __( 'Filter slides list', 'minimal210' ),
		);
		$args = array(
			'label'                 => __( 'Slide', 'minimal210' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'editor', 'thumbnail', 'author'),
			'taxonomies'            => array( 'slideshows' ),
			'hierarchical'          => false,
			'public'                => false,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'menu_icon'             => 'dashicons-format-gallery',
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => false,
			'can_export'            => true,
			'has_archive'           => false,		
			'exclude_from_search'   => true,
			'publicly_queryable'    => true,
			'capability_type'       => 'post',
		);
		register_post_type( 'slider', $args );
	}
	add_action( 'init', 'minimal_slider', 0 );
}

/*-------------------------------------------------------------------------------
	Slideshow taxonomy
-------------------------------------------------------------------------------*/

if ( ! function_exists( 'minimal_slideshow' ) ) {

	function minimal_slideshow() {

		$labels = array(
			'name'                       => __( 'Slideshows', 'minimal210' ),
			'singular_name'              => __( 'Slideshow', 'minimal210' ),
			'menu_name'                  => __( 'Slideshows', 'minimal210' ),
			'all_items'                  => __( 'All slideshows', 'minimal210' ),
			'parent_item'                => __( 'Parent slideshow', 'minimal210' ),
			'parent_item_colon'          => __( 'Parent slideshow:', 'minimal210' ),
			'new_item_name'              => __( 'New slideshow name', 'minimal210' ),
			'add_new_item'               => __( 'Add New slideshow', 'minimal210' ),
			'edit_item'                  => __( 'Edit slideshow', 'minimal210' ),
			'update_item'                => __( 'Update slideshow', 'minimal210' ),
			'view_item'                  => __( 'View slideshow', 'minimal210' ),
			'separate_items_with_commas' => __( 'Separate slideshows with commas', 'minimal210' ),
			'add_or_remove_items'        => __( 'Add or remove slideshows', 'minimal210' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'minimal210' ),
			'popular_items'              => __( 'Popular slideshows', 'minimal210' ),
			'search_items'               => __( 'Search slideshows', 'minimal210' ),
			'not_found'                  => __( 'Not Found', 'minimal210' ),
			'no_terms'                   => __( 'No slideshows', 'minimal210' ),
			'items_list'                 => __( 'Slideshows list', 'minimal210' ),
			'items_list_navigation'      => __( 'Slideshows list navigation', 'minimal210' ),
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => false,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => false,
		);
		register_taxonomy( 'slideshow', array( 'slider' ), $args );

	}
	add_action( 'init', 'minimal_slideshow', 0 );
}

/*-------------------------------------------------------------------------------
	Require ACF Slider settings
-------------------------------------------------------------------------------*/

require_once(get_template_directory(). '/inc/slider/settings.php');

/*-------------------------------------------------------------------------------
	Create shortcode
-------------------------------------------------------------------------------*/

if( ! function_exists('slider_shortcode') ) {

	function slider_shortcode( $atts ) {

		$atts = minimal_normalize_attributes($atts);
		$atts = shortcode_atts(array(
		            'slug' => ''
	    		), $atts);

	    if (term_exists($atts['slug'])) {

			$terms = get_terms( array(
				'taxonomy' => 'slideshow',
				'hide_empty' => true,
			) );

			$slides = array(
				'post_type' => 'slider',
				'posts_per_page' => -1,
				'tax_query' => array(
					array(
						'taxonomy' => 'slideshow',
						'field' => 'slug',
						'terms' => $atts['slug'],
					)
				)
			);

			$the_query = new WP_Query( $slides );

			if ( $the_query->have_posts() ) {

				wp_enqueue_style( 'minimal210-slider-style' );
				wp_enqueue_style( 'minimal210-theme-slider-style' );

				wp_enqueue_script( 'minimal210-slider-flexslider' );

				require( get_template_directory(). '/inc/slider/get-fields.php' );

				wp_enqueue_script('minimal210-slider-local');
				wp_localize_script('minimal210-slider-local', 'arg', $data);

				echo '<div id="main-slider">';
				echo '<div class="flexslider">';
				echo '<ul class="slides">';

				while ( $the_query->have_posts() ) { $the_query->the_post();

					// Get template file
					echo get_template_part( '/slider-templates/content','slider' );
				}

				echo '</ul>'; // Slides
				echo '</div>'; // Flexslider
				echo '</div>'; // Main-slider

				wp_reset_postdata();

			}else {
				// no posts found
			}
		}
	}
	add_shortcode( 'slider', 'slider_shortcode' );
}

/*-------------------------------------------------------------------------------
	Built-in do slider
-------------------------------------------------------------------------------*/

if( ! function_exists('minimal_do_slider') ) {

	function minimal_do_slider(){

	   	$term = get_field('selected_slider_name');

	   	if($term){

			$terms = get_terms( array(
				'taxonomy' => 'slideshow',
				'hide_empty' => true,
			) );

			$slides = array(
				'post_type' => 'slider',
				'posts_per_page' => -1,
				'tax_query' => array(
					array(
						'taxonomy' => 'slideshow',
						'field' => 'id',
						'terms' => $term ,
					)
				)
			);

			$the_query = new WP_Query( $slides );

			if ( $the_query->have_posts() ) {

				wp_enqueue_style( 'minimal210-slider-style' );
				wp_enqueue_style( 'minimal210-theme-slider-style' );

				wp_enqueue_script( 'minimal210-slider-flexslider' );

				require( get_template_directory(). '/inc/slider/get-fields.php' );

				wp_enqueue_script('minimal210-slider-local');
				wp_localize_script('minimal210-slider-local', 'arg', $data);

				echo '<div id="main-slider" class="'.$fit.'">';
				echo '<div class="flexslider">';
				echo '<ul class="slides">';

				while ( $the_query->have_posts() ) { $the_query->the_post();

					// Get template file
					echo get_template_part('/slider-templates/content','slider');
				}

				echo '</ul>'; // Slides
				echo '</div>'; // Flexslider
				echo '</div>'; // Main-slider

				wp_reset_postdata();

			}else {
			// no posts found
			}
		}
	}
}

/*-------------------------------------------------------------------------------
	Get slider image url
-------------------------------------------------------------------------------*/

if( ! function_exists('minimal_slider_image_url') ) {

	function minimal_slider_image_url($postid) {

		if($postid){

			// Geüploade afbeelding
			return get_the_post_thumbnail_url( $postid, 'full' );

			// Bijgesneden afbeelding
      		// echo wp_get_attachment_image_src( get_post_thumbnail_id( $postid ), 'full')[0];
		}
	}
}

/*-------------------------------------------------------------------------------
	Get slider image id
-------------------------------------------------------------------------------*/

if( ! function_exists('minimal_slider_image_id') ) {

	function minimal_slider_image_id() {

		echo get_post_thumbnail_id( $post->ID, 'full' );
	}
}


/*-------------------------------------------------------------------------------
	Slider featured image

	Needs attention. size on set
-------------------------------------------------------------------------------*/

if( ! function_exists( 'minimal_slider_image_size' ) ) {

	function minimal_slider_image_size( $thumb_size ){

	    if( 'slider' == get_post_type( esc_attr( $_GET['post'] ) ) ){

	        return array( 1000, 400 );
	    }
	    return array( 266, 266);
	}
	add_filter( 'admin_post_thumbnail_size', 'minimal_slider_image_size');
}

/*-------------------------------------------------------------------------------
	Create featured image for column
-------------------------------------------------------------------------------*/

if( ! function_exists( 'minimal_slider_featured_image_admin' ) ){

	function minimal_slider_featured_image_admin($post_ID) {

	    if (get_post_thumbnail_id($post_ID)) {

	        return get_the_post_thumbnail_url($post_ID,'featured_preview');
	    }
	}
}

/*-------------------------------------------------------------------------------
	Create column
-------------------------------------------------------------------------------*/

if( ! function_exists( 'minimal_image_column' ) ){

	function minimal_image_column($defaults) {

	    $defaults['featured_image'] = __('Slide image','minimal210');
	    return $defaults;
	}
}

/*-------------------------------------------------------------------------------
	Create column content
-------------------------------------------------------------------------------*/

if( ! function_exists( 'minimal_image_column_content' ) ){

	function minimal_image_column_content($column_name, $post_ID) {

	    if ($column_name == 'featured_image') {

	        $post_featured_image = minimal_slider_featured_image_admin($post_ID);

	        if ($post_featured_image) {

	            echo '<div style="background-size:cover;background-position:center;background-image: url('. $post_featured_image . '); height:100px; width:200px;" ></div>';
	        }
	    }
	}
}
add_filter('manage_slider_posts_columns', 'minimal_image_column');
add_action('manage_slider_posts_custom_column', 'minimal_image_column_content', 10, 2);
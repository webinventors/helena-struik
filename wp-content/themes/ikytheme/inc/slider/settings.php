<?php
/*-------------------------------------------------------------------------------
	Create ACF settings
-------------------------------------------------------------------------------*/

if( ! function_exists('minimal_slider_local_field_group') ) {

	function minimal_slider_local_field_group() {

		// Ensure translations have been loaded
		if( function_exists('acf_add_options_page') ) {
		
		    // Slider settings page
		    acf_add_options_sub_page(array(
				'page_title' 	=> __('Settings','minimal210'),
				'menu_title'	=> __('Settings','minimal210'),
				'parent_slug'	=> '/edit.php?post_type=slider',
				'capability'	=> 'manage_options',
			));

		    // Settings group general
			acf_add_local_field_group( array (
		        'key'      => 'slider_group',
		        'title'    => __('Slider settings','minimal210'),
		        'location' => array (
					array (
						array (
							'param' => 'options_page',
							'operator' => '==',
							'value' => 'acf-options-settings',
						),
					),
				),
		        'menu_order'            => 0,
		        'position'              => 'normal',
		        'style'                 => 'default',
		        'label_placement'       => 'top',
		        'instruction_placement' => 'label',
		        'hide_on_screen'        => '',
		    ) );

			// Tab 1: General settings
		    acf_add_local_field( array(
		    	'key'          	=> 'slider_tab_1_general_key',
		    	'label'        	=> __('General','minimal210'),
		    	'name'         	=> '',
		    	'type'         	=> 'tab',
		    	'placement'		=> 'top',
		    	'endpoint'		=> 0,
		    	'parent'       	=> 'slider_group',
		    	'required'     	=> 0,
		    ) );

		    // Animation type
	     	acf_add_local_field( array(
		        'key'          => 'slider_animation_type_key',
		        'label'        => __('Animation type','minimal210'),
		        'name'         => 'slider_animation_type_name',
		        'type'         => 'select',
		        'parent'       => 'slider_group',
		        'instructions' => __('Controls the animation type.','minimal210'),
		        'required'     => 0,
		        'choices' 	   => array( 'slide' => __('Slide','minimal210'), 'fade' => __('Fade','minimal210') ),
				'other_choice' => 0,
				'default_value'=> 'fade',
				'wrapper' => array (
					'width' => '50',
					'class' => '',
					'id' => '',
				),
		    ) );

	     	// Easing method
		    acf_add_local_field( array(
		        'key'          => 'slider_easing_key',
		        'label'        => __('Easing method','minimal210'),
		        'name'         => 'slider_easing_name',
		        'type'         => 'select',
		        'parent'       => 'slider_group',
		        'instructions' => __('Determines the easing method used in jQuery transitions.','minimal210'),
		        'required'     => 0,
		        'choices' 	   => array( 'swing' => __('Swing','minimal210'), 'linear' => __('Linear','minimal210') ),
				'other_choice' => 0,
				'default_value'=> 'swing',
				'wrapper' => array (
					'width' => '50',
					'class' => '',
					'id' => '',
				),
		    ) );

		    // Animation direction
		    // Only visible with Animation type slide
		    acf_add_local_field( array(
		        'key'          => 'slider_direction_key',
		        'label'        => __('Direction','minimal210'),
		        'name'         => 'slider_direction_name',
		        'type'         => 'select',
		        'parent'       => 'slider_group',
		        'instructions' => __('Controls the animation direction.','minimal210'),
		        'required'     => 0,
		        'choices' 	   => array( 'horizontal' => __('Horizontal','minimal210'), 'vertical' => __('Vertical','minimal210') ),
				'other_choice' => 0,
				'default_value'=> 'horizontal',
				'conditional_logic' => array (
					array (
						array (
							'field' => 'slider_animation_type_key',
							'operator' => '==',
							'value' => 'slide',
						),
					),
				),
				'wrapper' => array (
					'width' => '50',
					'class' => '',
					'id' => '',
				),
		    ) );

		    // Reverse
		    // Only visible with Animation type slide
		    acf_add_local_field( array(
		        'key'          => 'slider_reverse_key',
		        'label'        => __('Reverse animation','minimal210'),
		        'name'         => 'slider_reverse_name',
		        'type'         => 'checkbox',
		        'parent'       => 'slider_group',
		        'instructions' => __('Reverse the animation direction.','minimal210'),
		        'required'     => 0,
		        'choices' 	   => array( 'true' => __('Reverse','minimal210') ),
				'other_choice' => 0,
				'default_value'=> false,
				'conditional_logic' => array (
					array (
						array (
							'field' => 'slider_animation_type_key',
							'operator' => '==',
							'value' => 'slide',
						),
					),
				),
				'wrapper' => array (
					'width' => '50',
					'class' => '',
					'id' => '',
				),
		    ) );

		    // Enable bullets or thumbnails
		    acf_add_local_field( array(
		        'key'          => 'slider_controlnav_key',
		        'label'        => __( 'Pagination','minimal210' ),
		        'name'         => 'slider_controlnav_name',
		        'type'         => 'radio',
		        'parent'       => 'slider_group',
		        'instructions' => __( 'Create navigation for paging control of each slide.','minimal210' ),
		        'required'     => 0,
		        'choices' 	   => array( 'none' => __( 'None','minimal210' ), 'bullets' => __( 'Bullets','minimal210' ), 'thumbnails' => __( 'Thumbnails','minimal210' ) ),
				'other_choice' => 0,
				'default_value'=> 'true',
				'wrapper' => array (
					'width' => '50',
					'class' => '',
					'id' => '',
				),
		    ) );

		    // Fit thumbnails
		    // Only visible with ControlNav on thumbnails
		    acf_add_local_field( array(
		        'key'          => 'slider_thumbnails_fit_key',
		        'label'        => __( 'Automaticly fit the thumbnails horizontally.','minimal210' ),
		        'name'         => 'slider_thumbnails_fit_name',
		        'type'         => 'checkbox',
		        'parent'       => 'slider_group',
		        'instructions' => __( 'Use flexbox method to fit the thumbnails.','minimal210' ),
		        'required'     => 0,
		        'choices' 	   => array( 'true' => __( 'Enable','minimal210' ) ),
				'other_choice' => 0,
				'default_value'=> 'true',
				'conditional_logic' => array (
					array (
						array (
							'field' => 'slider_controlnav_key',
							'operator' => '==',
							'value' => 'thumbnails',
						),
					),
				),
				'wrapper' => array (
					'width' => '50',
					'class' => '',
					'id' => '',
				),
		    ) );

		    // Enable arrows
		    acf_add_local_field( array(
		        'key'          => 'slider_arrows_key',
		        'label'        => __('Arrows','minimal210'),
		        'name'         => 'slider_arrows_name',
		        'type'         => 'checkbox',
		        'parent'       => 'slider_group',
		        'instructions' => __('Create previous/next arrow navigation.','minimal210'),
		        'required'     => 0,
		        'choices' 	   => array( 'true' => __('Navigation arrows.','minimal210') ),
				'other_choice' => 0,
				'default_value'=> true,
		    ) );

		    // Previous text
		    // Only visible with Arrows true
		    acf_add_local_field( array(
		        'key'          => 'slider_navigation_prev_key',
		        'label'        => __('Previous text','minimal210'),
		        'name'         => 'slider_navigation_prev_name',
		        'type'         => 'text',
		        'parent'       => 'slider_group',
		        'instructions' => __('Set the text for the "previous" directionNav item.','minimal210'),
		        'required'     => 0,
				'default_value'=> '',
				'conditional_logic' => array (
					array (
						array (
							'field' => 'slider_arrows_key',
							'operator' => '==',
							'value' => 'true',
						),
					),
				),
				'wrapper' => array (
					'width' => '50',
					'class' => '',
					'id' => '',
				),
		    ) );

		    // Next text
		    // Only visible with Arrows true
		    acf_add_local_field( array(
		        'key'          => 'slider_navigation_next_key',
		        'label'        => __('Next text','minimal210'),
		        'name'         => 'slider_navigation_next_name',
		        'type'         => 'text',
		        'parent'       => 'slider_group',
		        'instructions' => __('Set the text for the "next" directionNav item.','minimal210'),
		        'required'     => 0,
				'default_value'=> '',
				'conditional_logic' => array (
					array (
						array (
							'field' => 'slider_arrows_key',
							'operator' => '==',
							'value' => 'true',
						),
					),
				),
				'wrapper' => array (
					'width' => '50',
					'class' => '',
					'id' => '',
				),
		    ) );	    

		    // Loop slides
		    acf_add_local_field( array(
		        'key'          => 'slider_loop_key',
		        'label'        => __('Infinite loop','minimal210'),
		        'name'         => 'slider_loop_name',
		        'type'         => 'checkbox',
		        'parent'       => 'slider_group',
		        'instructions' => __('Give the slider a seamless infinte loop.','minimal210'),
		        'required'     => 0,
		        'choices' 	   => array( 'true' => __('Loop','minimal210') ),
				'other_choice' => 0,
				'default_value'=> true
		    ) );

		    // Randomize order
		    acf_add_local_field( array(
		        'key'          => 'slider_randomize_key',
		        'label'        => __('Randomize slide order','minimal210'),
		        'name'         => 'slider_randomize_name',
		        'type'         => 'checkbox',
		        'parent'       => 'slider_group',
		        'required'     => 0,
		        'choices' 	   => array( 'true' => __('Randomize','minimal210') ),
				'other_choice' => 0,
				'default_value'=> false
		    ) );

		    // Set cycling speed
		    acf_add_local_field( array(
		        'key'          => 'slider_speed_key',
		        'label'        => __('Slideshow cycling speed','minimal210'),
		        'name'         => 'slider_speed_name',
		        'type'         => 'number',
		        'parent'       => 'slider_group',
		        'instructions' => __('Set the speed of the slideshow cycling, in milliseconds.','minimal210'),
		        'required'     => 0,
				'default_value'=> '7000',
				'min' => '500',
				'step' => '100',
				'wrapper' => array (
					'width' => '50',
					'class' => '',
					'id' => '',
				),
		    ) );

		    // Set animation speed
		    acf_add_local_field( array(
		        'key'         	=> 'slider_animationspeed_key',
		        'label'        	=> __('Slideshow animation speed','minimal210'),
		        'name'         	=> 'slider_animationspeed_name',
		        'type'         	=> 'number',
		        'parent'       	=> 'slider_group',
		        'instructions' 	=> __('Set the speed of animations, in milliseconds.','minimal210'),
		        'required'     	=> 0,
				'default_value'	=> '600',
				'min' 			=> '100',
				'step' 			=> '100',
				'wrapper' 		=> array (
					'width' 	=> '50',
					'class' 	=> '',
					'id' 		=> '',
				),
		    ) );

		    // Pause on hover
		    acf_add_local_field( array(
		        'key'          => 'slider_pause_key',
		        'label'        => __('Pause slider on hover','minimal210'),
		        'name'         => 'slider_pause_name',
		        'type'         => 'checkbox',
		        'parent'       => 'slider_group',
		        'instructions' => __('Pause the slideshow when hovering over slider, then resume when no longer hovering.','minimal210'),
		        'required'     => 0,
		        'choices' 	   => array( 'true' => __('Pause on hover','minimal210') ),
				'other_choice' => 0,
				'default_value'=> false
		    ) );

		    // Tab 2: Usability
		    acf_add_local_field( array(
		    	'key'          	=> 'slider_tab_2_usabillity_key',
		    	'label'        	=> __('Usability','minimal210'),
		    	'name'         	=> '',
		    	'type'         	=> 'tab',
		    	'placement'		=> 'top',
		    	'endpoint'		=> 0,
		    	'parent'       	=> 'slider_group',
		    	'required'     	=> 0,
		    ) );

		    // We dont want to see attachments in the next option
			$exclude_cpts = array(
			    'attachment',
			);

			// We only want the public post types
			$post_types = get_post_types( array(
			    'public'   => true,
			) );

			// Remove attachments from the others
			foreach($exclude_cpts as $exclude_cpt){
			    unset($post_types[$exclude_cpt]);
			}

			// If there are any left (should be at least pages and posts)
			if($post_types){

				// Loop the post types
				foreach ( $post_types  as $post_type ) {

					// Retrieve object
					$obj = get_post_type_object( $post_type );

					// Set default on true if its page
					if($post_type == 'page'){

						$default = true;
					}else{
						$default = false;
					}

					// Enable user metabox for post type
				    acf_add_local_field( array(
				        'key'          => 'slider_enable_user_metabox_'.$post_type.'_key',
				        'label'        => __('Enable &quot;Select slider&quot; for '.$obj->labels->name,'minimal210'),
				        'name'         => 'slider_enable_user_metabox_'.$post_type.'_name',
				        'type'         => 'checkbox',
				        'parent'       => 'slider_group',
				        'instructions' => __('&quot;Select slider&quot; metabox for the clients on the '.$obj->labels->name.' edit screen.','minimal210'),
				        'required'     => 0,
				        'choices' 	   => array( 'true' => __('Enable metabox','minimal210') ),
						'other_choice' => 0,
						'default_value'=> $default
				    ) );

				}
			}

			// Tab 3: Advanced
		    acf_add_local_field( array(
		    	'key'          	=> 'slider_tab_3_advanced_key',
		    	'label'        	=> __('Advanced','minimal210'),
		    	'name'         	=> '',
		    	'type'         	=> 'tab',
		    	'placement'		=> 'top',
		    	'endpoint'		=> 0,
		    	'parent'       	=> 'slider_group',
		    	'required'     	=> 0,
		    ) );

		    // Use CSS transitions
		    acf_add_local_field( array(
		        'key'          => 'slider_usecss_key',
		        'label'        => __('Use CSS3 transitions','minimal210'),
		        'name'         => 'slider_usecss_name',
		        'type'         => 'checkbox',
		        'parent'       => 'slider_group',
		        'instructions' => __('Slider will use CSS3 transitions, if available. (recommended)','minimal210'),
		        'required'     => 0,
		        'choices' 	   => array( 'true' => __('Use CSS','minimal210') ),
				'other_choice' => 0,
				'default_value'=> true
		    ) );

		    // Enable touch
		    acf_add_local_field( array(
		        'key'          => 'slider_touch_key',
		        'label'        => __('Touch swipe navigation','minimal210'),
		        'name'         => 'slider_touch_name',
		        'type'         => 'checkbox',
		        'parent'       => 'slider_group',
		        'instructions' => __('Allow touch swipe navigation of the slider on enabled devices.','minimal210'),
		        'required'     => 0,
		        'choices' 	   => array( 'true' => __('Enable touch navigation','minimal210') ),
				'other_choice' => 0,
				'default_value'=> true,
				'wrapper' => array (
					'width' => '50',
					'class' => '',
					'id' => '',
				),
		    ) );

		    // Enable keyboard
		    acf_add_local_field( array(
		        'key'          => 'slider_keyboard_key',
		        'label'        => __('Keyboard navigation','minimal210'),
		        'name'         => 'slider_keyboard_name',
		        'type'         => 'checkbox',
		        'parent'       => 'slider_group',
		        'instructions' => __('Allow slider navigation via keyboard left/right keys.','minimal210'),
		        'required'     => 0,
		        'choices' 	   => array( 'true' => __('Enable keyboard navigation','minimal210') ),
				'other_choice' => 0,
				'default_value'=> true,
				'wrapper' => array (
					'width' => '50',
					'class' => '',
					'id' => '',
				),
		    ) );

		    // Enable IE fade fix
		    // Only visible with Animation type fade
		    acf_add_local_field( array(
		        'key'          => 'slider_ie_fade_key',
		        'label'        => __('Internet Explorer Fade','minimal210'),
		        'name'         => 'slider_ie_fade_name',
		        'type'         => 'checkbox',
		        'parent'       => 'slider_group',
		        'instructions' => __('Custom self-made Internet Explorer fix for animation type: Fade.(recommended)','minimal210'),
		        'required'     => 0,
		        'choices' 	   => array( 'true' => __('Enable','minimal210') ),
				'other_choice' => 0,
				'default_value'=> true,
				'conditional_logic' => array (
					array (
						array (
							'field' => 'slider_animation_type_key',
							'operator' => '==',
							'value' => 'fade',
						),
					),
				),
		    ) );

		    // Custom controls
		    acf_add_local_field( array(
		        'key'          => 'slider_customcontrols_key',
		        'label'        => __('Custom controls container','minimal210'),
		        'name'         => 'slider_customcontrols_name',
		        'type'         => 'text',
		        'parent'       => 'slider_group',
		        'instructions' => __('jQuery Object/Selector Container the navigation elements should be appended to.','minimal210'),
		        'required'     => 0,
				'default_value'=> '',
				'wrapper' => array (
					'width' => '50',
					'class' => '',
					'id' => '',
				),
		    ) );

		    // Custom direction
		    acf_add_local_field( array(
		        'key'          => 'slider_customdirectionnav_key',
		        'label'        => __('Custom direction navigation','minimal210'),
		        'name'         => 'slider_customdirectionnav_name',
		        'type'         => 'text',
		        'parent'       => 'slider_group',
		        'instructions' => __('Container the custom navigation markup works with.','minimal210'),
		        'required'     => 0,
				'default_value'=> '',
				'wrapper' => array (
					'width' => '50',
					'class' => '',
					'id' => '',
				),
		    ) );

		    // Custom manual controls
		    acf_add_local_field( array(
		        'key'          => 'slider_manualcontrols_key',
		        'label'        => __('Custom Manual controls','minimal210'),
		        'name'         => 'slider_manualcontrols_name',
		        'type'         => 'text',
		        'parent'       => 'slider_group',
		        'instructions' => __('Geen idee wat hier moet komen. Klik <a href="https://github.com/woocommerce/FlexSlider/wiki/FlexSlider-Properties" target="_blank"> hier </a> voor meer informatie.','minimal210'),
		        'required'     => 0,
				'default_value'=> '',
				'wrapper' => array (
					'width' => '50',
					'class' => '',
					'id' => '',
				),
		    ) );

		    // Create metaboxes 
		    // Only visible on selected pages
			if($post_types){

				foreach ( $post_types  as $post_type ) {

				    if(get_field('slider_enable_user_metabox_'.$post_type.'_name','option') ) {

						$obj = get_post_type_object( $post_type );

				        // Page metabox 
					    acf_add_local_field_group( array (
					    	'key'					=> $post_type.'_slider_group',
					    	'title'					=> __('Select slideshow','minimal210'),
							'location' 				=> array (
								array (
									array (
										'param' 	=> 'post_type',
										'operator' 	=> '==',
										'value' 	=> $post_type,
									),
								),
							),
							'menu_order'			=> 99,
							'position'				=> 'side',
							'style'					=> 'default',
							'label_placement' 		=> 'top',
							'instruction_placement' => 'label',
							'hide_on_screen'		=> '',
					    ) );

					    acf_add_local_field( array( 
				    	    'key'          	=> 'selected_slider_field',
					        'label'        	=> __('Select slider','minimal210'),
					        'name'         	=> 'selected_slider_name',
					        'type'         	=> 'taxonomy',
					        'taxonomy'		=> 'slideshow',
					        'field_type'	=> 'radio',
					        'allow_null' 	=> 1,
					        'add_term'		=> 0,
					        'return_format' => 'id',
					        'multiple'		=> 0,
					        'parent'       	=> $post_type.'_slider_group',
					        'instructions' 	=> __('Selecteer jouw slideshow voor deze pagina.','minimal210'),
					    ));
					} // End page metabox
				} // End foreach
			} // End if post types are here
		} // End if ACF can add option pages
	}
	add_action('init', 'minimal_slider_local_field_group');
} ?>
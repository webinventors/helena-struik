<?php
define('WP_SCSS_ALWAYS_RECOMPILE', true);

function minimal210_enqueue_child_styles() {

	$parent_style = 'parent-style';

	wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css',	array( $parent_style ),	wp_get_theme()->get('Version') );
	
	wp_enqueue_style( 'minimal-scss-main-style', get_stylesheet_directory_uri(). '/css/main.css' );
	wp_enqueue_style( 'minimal-scss-responsive-style', get_stylesheet_directory_uri(). '/css/responsive.css' );

    wp_enqueue_script( 'objectfit', get_stylesheet_directory_uri(). '/scripts/objectfit.js', array('jquery') );
	wp_enqueue_script( 'custom-script', get_stylesheet_directory_uri(). '/scripts/scripts.js', array('jquery') );
}
add_action( 'wp_enqueue_scripts', 'minimal210_enqueue_child_styles' );

function minimal210_enqueue_child_admin_styles() {

	wp_enqueue_style( 'minimal210-child-admin-styles', get_stylesheet_directory_uri() . '/css/admin.css' );
}
add_action( 'admin_enqueue_scripts', 'minimal210_enqueue_child_admin_styles' );



//filter voor de p weg te halen in de contact form 7
add_filter('wpcf7_autop_or_not', '__return_false');


include ('wooti.php');

include ('functions-jasper.php');

?>

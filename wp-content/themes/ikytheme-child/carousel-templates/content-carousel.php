
<?php

$id 		= get_the_id();
$title 		= get_the_title();
$url 		= get_the_permalink();
$date 		= get_the_date();
$thumnbnail = get_the_post_thumbnail();

?>

<article class='carousel-inside'>

	<h2><a href="<?php echo $url; ?>"><?php echo $title; ?></a></h2>

</article>
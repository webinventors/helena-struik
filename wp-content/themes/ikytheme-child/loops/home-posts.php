<div class="item">

    <?php 
        if ( has_post_thumbnail() ) { ?>
            <div class="img" style="background-image:url(<?php the_post_thumbnail_url();?>);"></div>
        <?php } ?>

        <h3 class="title">
            <?=the_title()?>
        </h3>

        <div class="text">
            <?=the_excerpt()?>
        </div>

        <button>
            <a href="<?=the_permalink()?>">Lees meer</a>
        </button>

</div>
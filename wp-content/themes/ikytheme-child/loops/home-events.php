<?php 

$event          =   get_field('event'); //group->date, location, time_start, time_end, text
$event_button   =   get_field('event_all'); //show all events button

?>

<div class="event">
    <h5><?=the_title()?></h5>
    <div class="date">
        <?=$event['date']?> <span>van</span> <?=$event['time_start']?> <span>-</span> <?=$event['time_end']?>
    </div>
</div>
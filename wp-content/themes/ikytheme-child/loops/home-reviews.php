<?php

$review     =   get_field('review'); //name, text

?>

<div class="review">
    <div class="content">
        <?=$review['text']?>
        <div class="name"><?=$review['name']?></div>
    </div>
</div>
<?php
/* Template Name: Workshop */

//1st row content block 
$upper_content          =           get_field('block_upper');// title, content image

// content block in green image
$green_content          =           get_field('block_green');//title content leftside. and content right side

// content block na groene row
$personal_guidance      =           get_field('block_guidance');// image, title, content block 1 and content block 2

//content onderste block
$questions              =           get_field('block_questions');// title, content block 1 & 2

get_header();
?>
<div class="row top">
    <div class="full-row">
        <div class="blocks-container">
            
            <div class="block content">
                
                <h2 class="title"><?=$upper_content['title']?></h2>

                <p class="content"><?=$upper_content['content']?></p>
            
            </div>

            <div class="block image">
                <div class="image" style="background-image: url(<?=$upper_content['img']?>);">

                </div>
            </div>
        </div>
    </div>
</div>

<div class="row green" style="background-image: url('http://localhost:1337/helena/wp-content/uploads/2019/06/Path-436@2x.png');">
    <div class="full-row">
        <div class="blocks-container">

            <div class="block left-content">
                <h3 class="title"><?=$green_content['left_title']?></h3>

                <p class="content"><?=$green_content['left_content']?></p>
            </div>

            <div class="block right-content">
                <p class="right content-upper"><?=$green_content['content_right_upper']?></p>

                <p class="right content-lower"><?=$green_content['content_right_under']?></p>
            </div>

        </div>
    </div>
</div>

<div class="row after-green">
    <div class="full-row">
        <div class="blocks-container">
            <div class="block image">
                <div class="image" style="background-image: url('<?=$personal_guidance['img_left']?>');">

                </div>
            </div>

            <div class="block content">
                <h2 class="title"><?=$personal_guidance['title']?></h2>
                <p class="content-1"><?=$personal_guidance['text_block_1']?></p>
                <p class="content-2"><?=$personal_guidance['text_block_2']?></p>
            </div>

        </div>
    </div>
</div>

<div class="row questions">
    <div class="full-row">
        <div class="blocks-container">
            <div class="block questions">
                <h2 class="title"><?=$questions['title']?></h2>
                <p class="content block-1"><?=$questions['content_block_1']?></p>
                <p class="content block-2"><?=$questions['content_block_2']?></p>
            </div>
        </div>
    </div>
</div>

<?php
get_footer();
?>
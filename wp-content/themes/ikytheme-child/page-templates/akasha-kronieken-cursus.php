<?php

    /* Template Name: Akasha Kronieken Cursus */

$header             = get_field('cursus_header');
$header_content     = get_field('cursus_content');
$list               = get_field('possibilties_header');

$banner_header      = get_field('banner_header');
$banner_content     = get_field('banner_content');
$book_image         = get_field('banner_image');

$info_header        = get_field('info_header');
$info_content       = get_field('info_content');

$service_1          = get_field('service_1');
$service_2          = get_field('service_2');
$service_3          = get_field('service_3');
$service_4          = get_field('service_4');
$service_5          = get_field('service_5');


get_header();?>

<div class="row akasha-cursus">

    <div class="full-row">

        <div class="blocks-container">

            <div class="block">

                <div class="main-image" style="background-image:url('/helena/wp-content/uploads/2019/06/Group-72.png');">

                </div>

            </div>

            <div class="block">

                <div class="main-content">
                    <h2><?=$header?></h2>
                    <div class="wysiwyg">
                        <?=$header_content?>
                    </div>
                    <div class="white-space" style="height:25px;"></div>
                    <h2 class="leer"><?=$list?></h2>
                    <div class="possibilties">
                        <?php if(have_rows('possibilties_content')) : ?>
                            <ul class="list">
                                <?php while(have_rows('possibilties_content')) : the_row(); ?>

                                    <li><?php the_sub_field('possibilty')?></li>

                                <?php endwhile;?>
                            </ul>
                        <?php endif; ?>
                    </div>
                </div>

            </div>

        </div><!--blocks-container-->

    </div><!--full-row-->

</div><!--row-->

<div class="row product">

    <div class="full-row">

        <div class="blocks-container">

            <div class="block">

                <div class="product-content">
                    <h2><?=$banner_header?></h2>
                    <div class="wysiwyg">
                        <?=$banner_content?>
                    </div>
                    <button>
                        <a class="btn" href="#">Bekijk prijzen</a>
                    </button>
                </div>

            </div>

            <div class="block">

                <div class="image" style="background-image: url('<?=$book_image['url']?>');height:<?php echo $book_image['sizes']['large-height'] . 'px'?>;width:<?php echo $book_image['sizes']['large-width'] . 'px'?>;">

                </div>

            </div>

        </div><!--blocks-container-->

    </div><!--full-row-->

</div><!--row-->

<div class="row info">

    <div class="full-row">

        <div class="blocks-container">

            <div class="block">
                <h4><?=$info_header?></h4>
                <div class="wysiwyg">
                    <?=$info_content?>
                </div>
            </div>

        </div><!--blocks-container-->

    </div><!--full-row-->

</div><!--row-->

<div class="row options">

    <div class="full-row">

        <div class="blocks-container">

            <div class="block">
                <div class="option-wrapper">
                    <h2><?=$service_1['title']?></h2>
                    <p><?=$service_1['content_1']?></p>
                    <?php if($service_1['content_2']) { echo "<p>" . $service_1['content_2'] . "</p>";} ?>
                    <div class="price">€<?=$service_1['price']?> <span><?=$service_1['price_details']?></span></div>
                </div>
            </div>

            <div class="block">
                <div class="option-wrapper">
                    <h2><?=$service_2['title']?></h2>
                    <p><?=$service_2['content_1']?></p>
                    <?php if($service_2['content_2']) { echo "<p>" . $service_2['content_2'] . "</p>";} ?>
                    <div class="price">€<?=$service_2['price']?> <span><?=$service_2['price_details']?></span></div>
                </div>
            </div>

            <div class="block">
                <div class="option-wrapper">
                    <h2><?=$service_3['title']?></h2>
                    <p><?=$service_3['content_1']?></p>
                    <?php if($service_3['content_2']) { echo "<p>" . $service_3['content_2'] . "</p>";} ?>
                    <div class="price">€<?=$service_3['price']?> <span><?=$service_3['price_details']?></span></div>
                </div>
            </div>

        </div><!--blocks-container-->

        <div class="blocks-container">

            <div class="centering-div">
                <div class="block white-space"></div>
                <div class="block">
                    <div class="option-wrapper">
                        <h2><?=$service_4['title']?></h2>
                        <p><?=$service_4['content_1']?></p>
                        <?php if($service_4['content_2']) { echo "<p>" . $service_4['content_2'] . "</p>";} ?>
                        <div class="price">€<?=$service_4['price']?> <span><?=$service_4['price_details']?></span></div>
                    </div>
                </div>

                <div class="block">
                    <div class="option-wrapper">
                        <h2><?=$service_5['title']?></h2>
                        <p><?=$service_5['content_1']?></p>
                        <?php if($service_5['content_2']) { echo "<p>" . $service_5['content_2'] . "</p>";} ?>
                        <div class="price">€<?=$service_5['price']?> <span><?=$service_5['price_details']?></span></div>
                    </div>
                </div>
                <div class="block white-space"></div>
            </div>
        </div><!--blocks-container-->

    </div><!--full-row-->

</div><!--row-->


<?php
include('instructions-template.php');

include('contact-form.php');
?>

<?php get_footer(); ?>

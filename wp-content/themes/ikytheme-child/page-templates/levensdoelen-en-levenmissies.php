<?php

    /* Template Name: Levensdoelen en Levensmissies  */

$levensdoelen_header        = get_field('levensdoelen_header');
$levensdoelen_content       = get_field('levensdoelen_content');
$levensmissie_header        = get_field('levensmissie_header');
$levensmissie_content       = get_field('levensmissie_content');


$product_info               = get_field('product_info');
$product_desc               = get_field('product_description');

$price          = $product_info['price'];

$price = str_replace('.', ',', $price);

get_header();?>

<div class="row levensdoelen">
    <div class="full-row">
        <div class="blocks-container">
            <div class="block">
                <h3><?=$levensdoelen_header?></h3>
                <div class="wysiwyg">
                    <?=$levensdoelen_content?>
                </div>
                <h3><?=$levensmissie_header?></h3>
                <div class="wysiwyg">
                    <?=$levensmissie_content?>
                </div>
            </div>
            <div class="block">
                <div class="main-image"></div>
            </div>
        </div>
    </div>
</div>

<div class="row product">
    <div class="full-row">
        <div class="blocks-container">
            <div class="block">
                <div class="product-wrapper">
                    <div class="block">
                        <div class="image" style="background-image: url('<?=$product_info['image']['url']?>');"></div>
                    </div>
                    <div class="block">
                        <div class="info-wrapper">
                            <div class="title"><?=$product_info['title']?></div>
                            <div class="price">€<?=$price?></div>
                            <button><a href="<?=$product_info['link']['url']?>"><?=$product_info['link']['title']?></a></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="block">
                <div class="wysiwyg">
                    <?=$product_desc?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
include('contact-form.php');
?>



<?php get_footer();?>

<div class="row contact">
    <div class="full-row">
        <div class="blocks-container">
            <div class="block">
                <div class="title-content">
                    <div class="main">
                        <p>Bekijk onze diensten</p>
                    </div>
                    <div class="secondary">

                    </div>
                </div>
                <div class="block-content">
                    <ul class="services">
                        <?php
                        //Loops through the repeater
                        while (have_rows('services', 'option')) {
                            the_row();

                            //fetches the post object field. Assumes this is a single value, if they can select more than one you have to do a foreach loop here like the one in the example above
                            $post = get_sub_field('service');


                            $title = get_field('post_title', $post->ID);

                            echo "<a href='". get_permalink($post) . "'>" . "<li>" . get_the_title($post) ."</li>" . "</a>";


                        }
                        ?>
                    </ul>
                </div>
            </div><!--block-->
            <div class="block">
                <div class="contact">
                    <?php echo do_shortcode('[contact-form-7 id="48" title="Over mij"]')?>
                </div>
            </div><!--block-->
        </div><!--blocks-container-->
    </div><!--full-row-->
</div> <!--row-->

<?php
/* Template Name: Contact */

//eerste gegevens blok
$get_information            =           get_field('block_information');//tel nummer, email, straatnaam en postcode

//tweede gegevens blok
$get_details                =           get_field('block_office');//titel, dagen geopend, locatie

get_header();
?>
<div class="row contact-pagina">
    <div class="full-row">
        <div class="blocks-container">

            <div class="block content-upper">
                <div class="upper">
                    <p class="tel"><?=$get_information['tel']?></p>
                    <p class="email"><?=$get_information['email']?></p>
                </div>

                <div class="lower">
                    <p class="street"><?=$get_information['street']?></p>
                    <p class="postal"><?=$get_information['postal']?></p>
                </div>
            </div>

            <div class="block content-lower">
                <h2 class="title"><?=$get_details['title']?></h2>
                <p class="days"><?=$get_details['open']?></p>
                <p class="street"><?=$get_details['location']?></p>
            </div>

            <div class="facebook-dash">
                <i class="fa fa-facebook-f fa-3x"></i><p class="facebook">/happycacaorotterdam</p>
            </div>
            
            <div class="block form">
                <?php echo do_shortcode('[contact-form-7 id="577" title="Contact page form"]');?>

            </div>
            
        </div>
    </div>
</div>

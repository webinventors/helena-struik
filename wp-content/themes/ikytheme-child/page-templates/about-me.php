<?php
    /* Template Name: About Me */

$image          = get_field('main_aboutme_image');
$text           = get_field('main_aboutme_text');

$book_text      = get_field('book_text');
$book_image     = get_field('book_image');

$reviews_image  = get_field('reviews_image');
$reviews_text   = get_field('reviews_text');

$quote          = get_field('quote');

$summary_1      = get_field('summary_text_1');
$summary_2      = get_field('summary_text_2');
$summary_img    = get_field('summary_image');



get_header();?>

<div class="row about-me">
    <div class="full-row">
        <div class="blocks-container">
            <div class="block">
                <div class="main-image" style="background-image:url('/helena/wp-content/uploads/2019/06/Group-81.png');">
                    <div class="about-me-image" style="background-image: url('<?php echo $image['url']?>');height: <?php echo $image['sizes']['large-height'] . 'px';?>;">

                    </div>
                </div>
            </div><!--block-->
            <div class="block">
                <div class="block-content">
                    <h2>
                        <?=$text['header']?>
                    </h2>
                    <div class="wysiwyg">
                        <?=$text['Text']?>
                    </div><!--wysiwyg-->
                </div><!--block-content-->
            </div><!--block-->
        </div><!--blocks-container-->
    </div><!--full-row-->
</div> <!--row-->

<div class="row book">
    <div class="full-row">
        <div class="blocks-container">
            <div class="block">
                <div class="block-content">
                    <h2>
                        <?=$book_text['header']?>
                    </h2>
                    <div class="wysiwyg">
                        <?=$book_text['text']?>
                    </div><!--wysiwyg-->
                </div><!--block-content-->

            </div><!--block-->
            <div class="block">
                <div class="book-image">
                    <div class="image-backlayer" style="background-image: url('/helena/wp-content/uploads/2019/06/Group-185.png');height: 414px;">



                    </div>
                    <div class="image" style="background-image: url('<?=$book_image['url']?>');height:<?php echo $book_image['sizes']['large-height'] . 'px'?>;">

                        <div class="image-dot">

                        </div>

                    </div>

                </div>
            </div><!--block-->
        </div><!--blocks-container-->
    </div><!--full-row-->
</div> <!--row-->

<div class="row reviews">
    <div class="full-row">
        <div class="blocks-container">
            <div class="block">
                <div class="review-image">
                    <div class="image-backlayer" style="height: calc(<?php echo $reviews_image['sizes']['large-height'] . 'px'?> - 40px);">

                    </div>
                    <div class="image" style="background-image: url('<?=$reviews_image['url']?>');height:<?php echo $reviews_image['sizes']['large-height'] . 'px'?>;">

                    </div>
                </div>
            </div><!--block-->
            <div class="block">
                <div class="block-content">
                    <div class="wysiwyg  edit">
                        <?=$reviews_text?>
                    </div><!--wysiwyg-->
                </div><!--block-content-->
            </div><!--block-->
        </div><!--blocks-container-->
    </div><!--full-row-->
</div> <!--row-->

<div class="row quote">
    <div class="full-row">
        <div class="blocks-container">
            <div class="block">
                <div class="quote">
                    &quot;<?php echo $quote ?>&quot;
                </div>
            </div><!--block-->
        </div><!--blocks-container-->
    </div><!--full-row-->
</div> <!--row-->

<div class="row summary">
    <div class="full-row">
        <div class="blocks-container">
            <div class="block">
                <div class="block-content">
                    <div class="wysiwyg">
                        <?=$summary_1?>
                    </div>
                </div>
            </div><!--block-->
            <div class="block">
                <div class="big-image">
                    <div class="image" style="background-image: url('<?=$summary_img['url']?>');height:<?php echo $summary_img['sizes']['large-height'] . 'px'?>;width:<?php echo $summary_img['sizes']['large-width'] . 'px'?>;">

                    </div>
                </div>
                <div class="block-content right">
                    <div class="wysiwyg">
                        <?=$summary_2?>
                    </div>
                </div>
            </div><!--block-->
        </div><!--blocks-container-->
    </div><!--full-row-->
</div> <!--row-->

<?php
    include('contact-form.php');
?>



<?php get_footer();?>

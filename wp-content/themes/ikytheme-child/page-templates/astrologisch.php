<?php
/* Template Name: Astrologische-levenslessen */

// blok boven aan de pagina
$get_upper_content          =           get_field('block_top');// 2 titles, 2 content blocks an a image.

//groen content block
$get_repeater               =           get_field('block_green');// repeater

//over helena block
$get_about_helena           =           get_field('block_about');// titel and content for left block and title and content eand price for right side block

//data voor het de info row
$get_service_info           =           get_field('block_signup');// title, content upper and lower, email and tel

//repeater title
$get_repeater_title         =           get_field('title_services');

//repeater voor services    
$get_service_repeater       =           get_field('services');


get_header();
?>
<div class="row upper">
    <div class="full-row">
        <div class="blocks-container">
            <div class="block content">
                <h2 class="title upper"><?=$get_upper_content['title_top']?></h2>
                <p class="content top"><?=$get_upper_content['content_top']?></p>
                <h2 class="title under"><?=$get_upper_content['title_under']?></h2>
                <p class="content under"><?=$get_upper_content['content_under']?></p>
            </div>

            <div class="block image">
                <div class="image" style="background-image: url('<?=$get_upper_content['img']?>');">
                    
                </div>
            </div>

        </div>
    </div>
</div>

<div class="row repeater">
    <div class="full-row">
        <div class="blocks-container">
            <!-- repeater voor de groene blocken -->
            <?php if ( have_rows('block_green')):?>
                <?php while(have_rows('block_green')) : the_row();?>
                    <div class="green_block">
                        <div class="content-block">
                            <h2 class="title"><?php the_sub_field('title');?></h2>
                            <p class="content"><?php the_sub_field('content');?></p>
                        </div>
                        <div class="image-under" style="background-image: url('/helena/wp-content/uploads/2019/06/Path-303@2x-1.png');">
                        </div>
                    </div>
                <?php endwhile;?>  
            <?php else: ?>
                <?php echo'no rows found';?>
            <?php endif;?>
        </div>
    </div>
</div>

<div class="row about-helena">
    <div class="full-row">
        <div class="blocks-container">
            
            <div class="block about">
                <h2 class="title"><?=$get_about_helena['title']?></h2>
                <p class="content upper"><?=$get_about_helena['content_upper']?></p>
                <p class="content under"><?=$get_about_helena['content_under']?></p>
            </div>

            <div class="block life-lesson">
                <h2 class="title"><?=$get_about_helena['title_lesson']?></h2>
                <p class="content-lesson"><?=$get_about_helena['content_class']?></p>
                <p class="price-tag"><?=$get_about_helena['price']?></p>
            </div>

        </div>
    </div>
</div>

<?php include ('instructions-template.php');

get_footer();

?>
<?php

/* Template Name: Home */

get_header();


//Retrieve fields from Home Page
//Frontpage top
$bg_large           =       get_field('home_bg_large'); //img URL
$bg_text            =       get_field('home_bg_text');  //text
$bg_button          =       get_field('home_bg_button'); //link ARRAY

//About me row
$aboutme_photo      =       get_field('home_aboutme_photo'); //img URL
$aboutme_title      =       get_field('home_aboutme_title'); //text
$aboutme_text       =       get_field('home_aboutme_text'); //text
$aboutme_button     =       get_field('home_aboutme_button'); //link ARRAY

//Book row
$book_title         =       get_field('home_book_title'); //text
$book_text          =       get_field('home_book_text'); //wysiwyg
$book_button        =       get_field('home_book_button'); //link ARRAY
$book_img           =       get_field('home_book_img'); //img ARRAY

//Services row
$services_title     =       get_field('home_services_title');
$services_block_1   =       get_field('home_services_block_1'); //group icon,title,text,button
$services_block_2   =       get_field('home_services_block_2'); //group icon,title,text,button
$services_block_3   =       get_field('home_services_block_3'); //group icon,title,text,button

//Video row
$video_url          =       get_field('video_url'); //link URL
$video_content      =       get_field('video_content'); //group title, text, button

//Review row
$review_title       =       get_field('reviews_title');
$review             =       get_field('review'); //review_name, review_text

?>

<div class="row homepage" style="background-image:url(<?=$bg_large?>);">
    <div class="full-row">
        <div class="blocks-container">
            <div class="text">
                <p><?=$bg_text?></p>
            </div>
            <button>
                <a href="<?=$bg_button['url']?>">
                    <?=$bg_button['title']?>
                </a>
            </button>
        </div>
    </div>
</div> 

<div class="row aboutme">
    <div class="full-row">
        <div class="blocks-container">

            <div class="picture-container">
                <div class="profile-picture" style="background-image:url(<?=$aboutme_photo?>);">
                </div>
            </div>

            <div class="title">
                <p><?=$aboutme_title?></p>
            </div>

            <div class="text">
                <p><?=$aboutme_text?></p>
            </div>

            <button>
                <a href="<?=$aboutme_button['url']?>">
                    <?=$aboutme_button['title']?>
                </a>
            </button>

        </div>
    </div>
</div> 

<div class="row books">
    <div class="full-row">
        <div class="blocks-container">

            <div class="text-block">
                <div class="content">
                    <h2><?=$book_title?></h2>
                    <?=$book_text?>
                    <button>
                        <a href="<?=$book_button['url']?>">
                            <?=$book_button['title']?>
                        </a>
                    </button>
                </div>
            </div>

            <div class="book-image">
                <img src="<?=$book_img['url']?>" alt="<?=$book_img['alt']?>">
            </div>
        </div>
    </div>
</div>

<div class="row home-services">
    <div class="full-row">
        <h3><?=$services_title?></h3>
        <div class="blocks-container">
            

        <div class="article">
            <div class="content">
                <?=$services_block_1['icon']?>
                <h3><?=$services_block_1['title']?></h3>
                <?=$services_block_1['text']?>
                <button>
                    <a href="<?=$services_block_1['button']['url'];?>">
                        <?=$services_block_1['button']['title'];?>
                    </a>
                </button>
            </div>
        </div>

        <div class="article">
            <div class="content">
            <?=$services_block_2['icon']?>
                <h3><?=$services_block_2['title']?></h3>
                <?=$services_block_2['text']?>
                <button>
                    <a href="<?=$services_block_2['button']['url'];?>">
                        <?=$services_block_2['button']['title'];?>
                    </a>
                </button>
            </div>
            </div>

        <div class="article">
            <div class="content">
            <?=$services_block_3['icon']?>
                <h3><?=$services_block_3['title']?></h3>
                <?=$services_block_3['text']?>
                <button>
                    <a href="<?=$services_block_3['button']['url'];?>">
                        <?=$services_block_3['button']['title'];?>
                    </a>
                </button>
            </div>
            </div>
        </div>
    </div>
</div>

<div class="row home-posts">
    <div class="full-row">
        <div class="blocks-container">
        <?php 
    // WP_Query arguments
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

    $args = array(
        'post_type'              => array( 'post' ),
        'posts_per_page'         => '3',
        'order'			         => 'DESC'
    );

    // The Query
    $query = new WP_Query( $args );

    // The Loop
    if ( $query->have_posts() ) { ?>
        <?php while ( $query->have_posts() ) { $query->the_post();
            include(locate_template('/loops/home-posts.php') );                       

        } ?>
            
    <?php
        // Restore original Post Data
        wp_reset_postdata(); ?>
    <?php } ?>

        </div> 
    </div>
</div>

<div class="row home-video">
    <div class="full-row">
        <div class="blocks-container">

            <div class="video">
                <?php echo $video_url; ?>
            </div>

            <div class="text">
                <div class="content">
                    <h3 class="title">
                        <?=$video_content['title']?>
                    </h3>
                    <?=$video_content['text']?>
                    <button>
                        <a href="<?=$video_content['button']['url']?>">
                            <?=$video_content['button']['title']?>
                        </a>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row home-reviews">
    <div class="full-row">
    <h2><?=$review_title?></h2>
        <div class="blocks-container">
            
        <?php 
    // WP_Query arguments
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

    $args = array(
        'post_type'              => array( 'ervaringen' ),
        'posts_per_page'         => '3',
        'order'			         => 'DESC'
    );

    // The Query
    $query = new WP_Query( $args );

    // The Loop
    if ( $query->have_posts() ) { ?>
        <?php while ( $query->have_posts() ) { $query->the_post();
            include(locate_template('/loops/home-reviews.php') );                       

        } ?>
            
    <?php
        // Restore original Post Data
        wp_reset_postdata(); ?>
    <?php } ?>

        </div>
    </div>
</div>

<div class="row contact-events">
    <div class="full-row">
        <div class="blocks-container">

            <div class="contact">
                <?php echo do_shortcode('[contact-form-7 id="612" title="Home contact"]')?>
            </div>

            <div class="events">
                <h5>Evenementen</h5>
                <?php 
                    // WP_Query arguments
                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                    $args = array(
                        'post_type'              => array( 'evenementen' ),
                        'posts_per_page'         => '3',
                        'order'			         => 'DESC'
                    );

                    // The Query
                    $query = new WP_Query( $args );

                    // The Loop
                    if ( $query->have_posts() ) { ?>
                        <?php while ( $query->have_posts() ) { $query->the_post();
                            include(locate_template('/loops/home-events.php') );                       

                        } ?>
                            
                    <?php
                        // Restore original Post Data
                        wp_reset_postdata(); ?>
                    <?php } ?>
                <button>
                    <a href="#">
                        Alle evenementen
                    </a>
                </button>
            </div>
        </div>
    </div>
</div>



<?php get_footer(); ?>
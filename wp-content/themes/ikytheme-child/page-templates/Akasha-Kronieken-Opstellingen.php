<?php
$header                      = get_field('headertitel'); //text
$header_content1             = get_field('headertext'); //text
$header_content2             = get_field('headertext1'); //text

$header2                      = get_field('wat_zijn_akasha_titel'); //text
$header_content3              = get_field('Wat_ zijn_ Akasha_text'); //text
$header_afbeelding            = get_field('header_afbeelding'); //array

$midheader                    = get_field('midheadertitel'); //text
$midheader2                   = get_field('midheadertitel2'); //text
$midheader3                   = get_field('midheadertitel3'); //text

$midheader_content            = get_field('midheader_content'); //text
$midheader_content2           = get_field('midheader_content2'); //text
$midheader_content3           = get_field('midheader_content3'); //text

$diensten1                    = get_field('diensten_one');
$diensten2                    = get_field('diensten_two');
    /* Template Name: Akasha Kronieken Opstellingen */

    get_header();
    ?>

<div class="row akasha-opstelling">
     <div class="full-row">
         <div class="blocks-container">
            <div class="afbeelding">
              <img src="<?=$header_afbeelding['url']?>" alt="<?=$header_afbeelding['alt']?>">
            </div>
             <div class="blok">
                 <h2 class="titel"><?=$header?></h2>
                        <div class="header-content">
                        <div class="contant1"><?=$header_content1?></div><br>
                        <div class="contant2"><?=$header_content2?></div>
                    </div>
             </div>
             <div class="blok2">
                 <h2 class="titel"><?=$header2?></h2>
                        <div class="header-content1">
                        <?=$header_content3?><br>
                    </div>
             </div>
        </div><!--blocks-container-->
    </div>
</div>
</div>




<div class="row mid_container">
    <div class="full-row_mid">
            <div class="blok">
                 <h2 class="titel"><?=$midheader?></h2>
                        <div class="mid-content"></div>
                        <div class="midcontant"><?=$midheader_content?></div>
                  </div> 
                 <div class="blok2">
                    <h2 class="titel"><?=$midheader2?></h2>
                        <div class="mid-content2"></div>
                        <div class="midcontant2"><?=$midheader_content2?></div>
                     </div>
                  <div class="blok3">
                    <h2 class="titel"><?=$midheader3?></h2>
                        <div class="mid-content3">
                        <div class="midcontant3"><?=$midheader_content3?></div>
                     </div>
                  </div>
    </div>
     </div><!--mid-container-->
  </div>
</div>

<div class="row bottom_container">
    <div class="full-row">
        <div class="blocks-container">
            <div class="blok">
            <div class="option-wrapper">
                    <h2><?=$diensten1['titel']?></h2>
                    <p><?=$diensten1['inhoud']?></p>
                    <div class="price"><?=$diensten1['price']?></div>
             </div>
</div>
            <div class="blok2">
            <div class="option-wrapper">
                    <h2><?=$diensten2['titel']?></h2>
                    <p><?=$diensten2['inhoud']?></p>
                    <div class="price"><?=$diensten2['price']?></div>
                </div>
                     </div>
         </div>
     </div><!--mid-container-->
  </div>
</div>
<?php
include('instructions-template.php');
?>

<?php get_footer(); ?>

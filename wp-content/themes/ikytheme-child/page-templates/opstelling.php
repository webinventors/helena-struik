<?php
/* Template Name: opstelling */
//content en afbeelding voor de row setup
$get_content_top            =           get_field('block_top'); 
//content en titles voor de grey bar
$get_grey_content           =           get_field('block_questions');
$content_right              =           get_field('content_right');
//tijd, beschrijving en prijs voor consulting blok links en rechts
$get_consult                =           get_field('block_prices');
get_header();
?>

<div class="row setup">
    <div class="full-row">
        <div class="blocks-container">

            <div class="block image">
                <div class="image" style="background-image: url('<?=$get_content_top['img']?>');">
                
                </div>
            </div>

            <div class="block content">
                <div class="upper">
                    <h2 class="title"><?=$get_content_top['title']?></h2>
                    <p><?=$get_content_top['content_verse1']?></p>
                    <p><?=$get_content_top['content_verse2']?></p>
                </div>

                <div class="lower">
                    <h2 class="title"><?=$get_content_top['title_two']?></h2>
                    <p><?=$get_content_top['content']?></p>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="row grey-swipe" style="background-image: url('/helena/wp-content/uploads/2019/06/Path-342.png');">
    <div class="full-row">
        <div class="blocks-container">
            <div class="block left top">
                <h2 class="title"><?=$get_grey_content['title_left']?></h2>
                <p class="content"><?=$get_grey_content['content_left']?></p>
            </div>

            <div class="block right">
                <h2 class="title"><?=$get_grey_content['title_right']?></h2>
                <?=$content_right?>
            </div>

            <div class="block left bottom">
                <h2 class="title"><?=$get_grey_content['title_left_under']?></h2>
                <p class="content"><?=$get_grey_content['content_left_under']?></p>
            </div>

            <div class="block consulting">
                <div class="consult left">
                    <h2 class="title"><?=$get_consult['time_left']?></h2>
                    <p class="content"><i class="fa fa-check fa-xs"></i><?=$get_consult['description_left']?></p>
                    <div class="button">
                        <?=$get_consult['price_left']?>
                    </div>
                </div>

                <div class="consult right">
                    <h2 class="title"><?=$get_consult['time_right']?></h2>
                    <p class="content"><i class="fa fa-check fa-xs"></i><?=$get_consult['description_right']?></p>
                    <div class="button">
                        <?=$get_consult['price_right']?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
include ('instructions-template.php');

get_footer();
?>
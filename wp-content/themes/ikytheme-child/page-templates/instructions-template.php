<?php
$instructions_header    =   get_field('instructions_header');
$instructions_content   =   get_field('instructions_content');
?>

<div class="row instructions">

    <div class="full-row">

        <div class="blocks-container">
            <div class="block">
                <h2><?=$instructions_header?></h2>
                <div class="wysiwyg">
                    <?=$instructions_content?>
                </div>
            </div>

            <div class="block more-services">
                <h2>Meer diensten</h2>
                <ul class="instructions-services">
                    <?php
                    //Loops through the repeater
                    while (have_rows('services', 'option')) {
                        the_row();

                        //fetches the post object field. Assumes this is a single value, if they can select more than one you have to do a foreach loop here like the one in the example above
                        $post = get_sub_field('service');


                        $title = get_field('post_title', $post->ID);

                        echo "<a href='". get_permalink($post) . "'>" . "<li>" . get_the_title($post) ."</li>" . "</a>";

                    }
                    ?>
                </ul>
            </div>
        </div><!--blocks-container-->

    </div><!--full-row-->

</div><!--row-->

<?php

/* Template Name: Healing */

// veel content voor de bovenste tekst row 
$get_upper_content              =               get_field('block_upper');// titel en content voor links boven, rechts boven en links onder in de desbetreffende row

//repeater voor de consults
$get_repeater                   =               get_field('block_consulting');//repeater

//gegevens voor het inschrijven block
$get_register                   =               get_field('block_register');// title, content, email en tel nr 

//titel en repeater voor het meerdere diensten block
$get_title                      =               get_field('title');// title
$get_service_repeater           =               get_field('services');//repeater met content

//vragen block 
$get_questions                  =               get_field('block_questions'); //3 titles en 3 content blocken 
$get_questions_repeater         =               get_field('insight_repeater'); //repeater met 1 content veld

get_header();
?>
<div class="row healing-intro">
    <div class="full-row">
        <div class="blocks-container">
            <!-- linker content row -->
            <div class="block content">
                <h2><?=$get_upper_content['title_upper_left']?></h2>
                <p class="content"><?=$get_upper_content['content_upper_left']?></p>
                <h2><?=$get_upper_content['title_under_left']?></h2>
                <p class="content"><?=$get_upper_content['content_under_left']?></p>
            </div>
            <!-- rechter content row -->
            <div class="block more-content">
                <h2><?=$get_upper_content['title_upper_right']?></h2>
                <p><?=$get_upper_content['content_upper_right']?></p>
                <div class="image-bounderies">
                    <div class="image" style="background-image: url('<?=$get_upper_content['img']?>');">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row consulting">
    <div class="full-row">
        <div class="blocks-container">
            <div class="block consulting">
                <!-- repeater voor consults -->
                <?php if ( have_rows('block_consulting')):?>
                    <?php while(have_rows('block_consulting')) : the_row();?>
                        <div class="repeat">
                            <p class="content"><?php the_sub_field('consulting');?></p>
                            <p class="price"><?php the_sub_field('price');?></p>
                        </div>        
                    <?php endwhile;?>
                <?php else: ?>
                    <?php echo'no rows found';?>
                <?php endif;?>

            </div>

        </div>
    </div>
</div>

<div class="row register">
    <div class="full-row">
        <div class="blocks-container">
            <div class="block register">
                <div class="pouch">
                    <h2 class="title"><?=$get_register['title']?></h2>
                    <p class="content"><?=$get_register['content']?></p>

                    <h3 class="email"><?=$get_register['email']?></h3>
                    <p class="tel"><?=$get_register['tel']?></p>
                </div>
            </div>

            <div class="block service-repeater">
            <h2 class="title-repeater"><?=$get_title?></h2>
            <!-- repeater voor services -->
                <div class="services">
                    <?php if ( have_rows('services')):?>
                        <?php while(have_rows('services')) : the_row();?>

                                <p class="content"><?php the_sub_field('content');?></p>

                        <?php endwhile;?>
                    <?php else: ?>
                        <?php echo'no rows found';?>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row summary">
    <div class="full-row">
        <div class="blocks-container">
            <div class="block situation">
                <h2 class="title"><?=$get_questions['title_insight']?></h2>
                <p class="content"><?=$get_questions['content_insight']?></p>
            </div>

            <div class="block healing">
                <h2 class="title"><?=$get_questions['title_questions']?></h2>
                <p class="questions verse-1"><?=$get_questions['content_questions_verse1']?></p>
                <p class="questions verse-2"><?=$get_questions['content_questions_verse2']?></p>
            </div>

            <div class="block summary-repeater">
                <P class="little-title"><?=$get_questions['content_insight_question']?></P>
                   <?php if ( have_rows('insight_repeater')):?>
                       <?php while(have_rows('insight_repeater')) : the_row();?>
                            <p class="content"><?php the_sub_field('content');?></p>
                       <?php endwhile;?>
                   <?php else: ?>
                       <?php echo'no rows found';?>
                   <?php endif;?>
            </div>

            <div class="block do-it">
                <h2 class="title"><?=$get_questions['title_do']?></h2>
                <p class="do"><?=$get_questions['content_do']?></p>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();
?>
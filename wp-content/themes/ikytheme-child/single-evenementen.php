<?php get_header(); ?>



<?php if(function_exists('minimal_do_slider') ) minimal_do_slider(); ?>

    <main>

        <?php while ( have_posts() ) : the_post(); ?>

            <?php
                $event = get_field('event');
                $price = $event['price'];
                $price = str_replace('.', ',', $price);
            ?>

            <div class='row event-text'>

                <div class='full-row'>

                    <div class='blocks-container'>

                        <div class='block'>

                            <h1><?php the_title(); ?></h1>
                            <div class="info">
                                <h3 class="price">€<?=$price?></h3>
                                <h3><?=$event['date']?></h3>
                                <h3><?=$event['time_start']?> - <?=$event['time_end']?></h3>
                            </div>
                            <h3 class="location"><?=$event['location'] ?></h3>


                            <?=$event['text']?>
                        </div>

                        <div class="block">

                            <div class="image" style="background-image: url('<?=$event['image']['url']?>');">

                            </div>

                        </div>

                    </div> <!-- blocks-container -->

                </div> <!-- full-row -->

            </div> <!-- row -->


            <?php include('page-templates/contact-form.php'); ?>


        <?php endwhile; // end of the loop. ?>

    </main>

<?php get_footer(); ?>

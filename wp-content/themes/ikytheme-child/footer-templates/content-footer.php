<?php
//Block logo 
$get_logo_block                 =           get_field('block_logo', 'option'); //logo footer

//contact blok gegevens
$get_contact_block              =           get_field('contact_block', 'option'); //tel nr, email, adress 

//locatie block gegevens
$get_location_block             =           get_field('location_block', 'option');// title, openingstijden, straat, stad


?>

<footer id='main-footer'>
    <div class="row footer">
        <div class="full-row">
            <div class="blocks-container">

                <div class="block logo">
                    <a href="<?php echo home_url();?>">
                        <div class="logo" style="background-image:url('<?=$get_logo_block['logo']?>')">    
                        </div>  
                    </a>
                </div>

                <div class="block intel">
                    <a href="tel:<?=$get_contact_block['phone']?>">
                        <p class="tel"><?=$get_contact_block['phone']?></p>
                    </a>

                    <a href="mailto:<?=$get_contact_block['email']?>">
                        <p class="mail"><?=$get_contact_block['email']?></p>
                    </a>

                    <p class="location"><?=$get_contact_block['street_number']?></p>
                    <p class="city"><?=$get_contact_block['city_postal']?></p>
                    
                </div>

                <div class="block location">
                    <div class="content">

                        <h2 class="title"><?=$get_location_block['title']?></h2>

                        <p class="open"><?=$get_location_block['open']?></p>

                        <p class="location street"><?=$get_location_block['street'] ?></p>

                        <p class="location postal"><?=$get_location_block['postal'] ?></p>

                    </div>

                    <div class="maps" style="background-image: url('<?=$get_location_block ['maps']?>')">

                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="row copyright">
        <div class="full-row">
            <div class="blocks-container">
                <div class="block copyright">
                    <a href="https://www.ikycreative.nl/" class="copyright">&copy;iKy creative 2019</a>
                </div>
            </div>
        </div>
    </div>
    <?php min_responsive_menu('Main-menu'); ?>
</footer>

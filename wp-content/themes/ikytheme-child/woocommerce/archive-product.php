<?php


get_header();?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <div class="row title">
            <div class="full-row">
                <div class="blocks-container">

                    <h1>Shop</h1>
                </div>
            </div>
        </div>
        <div class="row items">
            <div class="full-row">
                <div class="blocks-container">
                    <div class="block"><?php echo do_shortcode('[product_categories number="12" parent="0"]');?></div>
                    <div class="block">
                        <?php echo do_shortcode('[woof sid=”custom” autosubmit=1]');?>
                        <?php echo do_shortcode('[woof_products per_page=12 columns=3 is_ajax=1 taxonomies=product_cat:9]') ?>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>


<?php get_footer();?>

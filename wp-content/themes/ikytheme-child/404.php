<?php




get_header(); ?>

<main>

	<div class='row error-row'>
		
		<div class='full-row'>
			
			<div class='blocks-container'>
				
				<div class='block'>
                    <div class="error-wrapper">
                        <div class="error-content">
                            <h1>404</h1>
                            <h2>Pagina niet gevonden</h2>
                            <p>Oops! De pagina die u zoekt is niet beschikbaar. Ga naar een andere pagina of neem contact met ons op.</p>
                        </div>
                    </div>
				</div>

                <div class='block'>
                    <div class="services-wrapper">
                        <h3>Bent u misschien op zoek naar:</h3>
                        <?php if(have_rows('services', 'option')) : ?>
                            <ul class="services">
                                <?php while(have_rows('services', 'option')) : the_row(); ?>

                                    <li><?php the_sub_field('service')?></li>

                                <?php endwhile;?>
                            </ul>
                        <?php endif; ?>

                    </div>
                </div>

			</div> <!-- blocks-container -->

		</div> <!-- full-row -->

	</div> <!-- row -->

</main>

<?php get_footer(); ?>

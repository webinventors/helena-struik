<?php

add_action('template_redirect', 'remove_shop_breadcrumbs' );
function remove_shop_breadcrumbs(){
    remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0);

}

add_action( 'woocommerce_product_thumbnails', 'bbloomer_custom_action', 10 );

function bbloomer_custom_action() {
    echo "<div class='image-1'></div>";
    echo "<div class='image-2'></div>";
    echo "<div class='image-3'></div>";
}


/**
 * Rename product data tabs
 */
add_filter( 'woocommerce_product_tabs', 'woo_rename_tabs', 98 );
function woo_rename_tabs( $tabs ) {

    $tabs['description']['title'] = __( 'Beschrijving' );		// Rename the description tab
    $tabs['reviews']['title'] = __( 'Beoordelingen' );				// Rename the reviews tab
    $tabs['additional_information']['title'] = __( 'Extra informatie' );	// Rename the additional information tab

    return $tabs;

}

/**
 * Reorder product data tabs
 */
add_filter( 'woocommerce_product_tabs', 'woo_reorder_tabs', 98 );
function woo_reorder_tabs( $tabs ) {

    $tabs['reviews']['priority'] = 10;			// Reviews second
    $tabs['description']['priority'] = 5;			// Description first
    $tabs['additional_information']['priority'] = 15;	// Additional information third

    return $tabs;
}

//add_action( 'woocommerce_after_shop_loop_item', function(){
//    remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart' );
//}, 1 );

// Replace add to cart button by a linked button to the product in Shop and archives pages
add_filter( 'woocommerce_loop_add_to_cart_link', 'replace_loop_add_to_cart_button', 10, 2 );
function replace_loop_add_to_cart_button( $button, $product  ) {
    // Not needed for variable products
    if( $product->is_type( 'variable' ) ) return $button;

    // Button text here
    $button_text = __( "Bekijk nu", "woocommerce" );

    return '<a class="button" href="' . $product->get_permalink() . '">' . $button_text . '</a>';
}

add_filter('woocommerce_catalog_orderby', 'wc_customize_product_sorting');

function wc_customize_product_sorting($sorting_options){
    $sorting_options = array(
        'menu_order' => __( 'Sorteer op', 'woocommerce' ),
        'popularity' => __( 'Sorteer op populariteit', 'woocommerce' ),
        'rating'     => __( 'Sorteer op score', 'woocommerce' ),
        'date'       => __( 'Sorteer op recentie', 'woocommerce' ),
        'price'      => __( 'Sorteer op prijs: laag naar hoog', 'woocommerce' ),
        'price-desc' => __( 'Sorteer op prijs: hoog naar laag', 'woocommerce' ),
    );

    return $sorting_options;
}

add_filter( 'woocommerce_product_description_heading', '__return_null' );
?>

<?php get_header(); ?>

<?php echo do_shortcode('[slider slug=""]'); ?>

<main>

	<div class='row'>
		
		<div class='full-row'>
			
			<div class='blocks-container'>
				
				<div class='block page-title'>
					
					<h1><?php printf( esc_html__( 'Zoekresultaten voor: %s', 'minimal210' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
					
				</div>

			</div> <!-- blocks-container -->

		</div> <!-- full-row -->

	</div> <!-- row -->

	<div class='row search-results'>
		
		<div class='full-row'>
			
			<div class='blocks-container'>
				
				<div class='block'>

				<?php if ( have_posts() ) { ?>
					
					<?php while( have_posts() ) { the_post();

						$url 		= get_the_permalink();
						$id 		= get_the_id();
						$title 		= get_the_title();
						$date 		= get_the_date();
						$thumnbnail = get_the_post_thumbnail();
						// $content 	= get_field( 'search_text' );
						// http://minimal.buro210.nl/functions/custom-fields-excerpt

						?>

						<article>

							<div class='search-title'>
								
								<h2><a href="<?php echo $url; ?>"><?php echo $title; ?></a></h2>

							</div>

							<div class='search-content'>
								
								<?php echo $content; ?>

							</div>
							
						</article>

					<?php }

					minimal_pagination() ?>

				<?php }else{ ?>

                    <h3><?php echo __( 'Geen resultaten gevonden','minimal210' ); ?></h3>
                    <p>Klik <a href='/'>hier</a> om terug te keren naar de homepagina.</p>

				<?php } ?>

				</div>

			</div> <!-- blocks-container -->

		</div> <!-- full-row -->

	</div> <!-- row -->

</main>

<?php get_footer(); ?>
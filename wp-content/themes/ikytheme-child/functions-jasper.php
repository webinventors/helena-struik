<?php

function register_my_menus() {
    register_nav_menus(
      array(
        'main-menu' => __( 'Hoofd Menu' )
      )
    );
  }
  add_action( 'init', 'register_my_menus' );

function remove_admin_login_header() {
	remove_action('wp_head', '_admin_bar_bump_cb');
}
add_action('get_header', 'remove_admin_login_header');

/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
  return 33;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

// Our custom post type function
function create_posttype() {
 
    register_post_type( 'ervaringen',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Ervaringen' ),
                'singular_name' => __( 'Ervaring' ),
                'add_new'            => _x( 'Nieuwe Ervaring', 'book', 'minimal210' ),
                'add_new_item'       => __( 'Voeg nieuwe Ervaring toe', 'minimal210' )
            ),
            'public' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => 'ervaringen'),
        )
    );

    register_post_type( 'evenementen',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Evenementen' ),
                'singular_name' => __( 'Evenement' ),
                'add_new'            => _x( 'Nieuw Evenement', 'book', 'minimal210' ),
                'add_new_item'       => __( 'Voeg nieuw Evenement toe', 'minimal210' )
            ),
            'public' => true,
            'has_archive' => false,
            'rewrite' => array('slug' => 'evenementen'),
        )
    );

}

// Hooking up our function to theme setup
add_action( 'init', 'create_posttype' );



 function translate_strings( $translated, $text, $domain ) {
 
  // STRING 1
  $translated = str_ireplace( 'november', 'November', $translated );
 
  // STRING 2
  $translated = str_ireplace( 'augustus', 'Augustus', $translated );
 
return $translated;

}

add_filter( 'gettext', 'translate_strings', 999, 3 );

?>
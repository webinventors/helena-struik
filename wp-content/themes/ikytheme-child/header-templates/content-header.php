<?php

// Retrieve settings from settings page
$sticky_menu = get_field('setting_sticky_header_name','option');

if($sticky_menu){

	$sticky_class = 'sticky-menu';
}else{

	$sticky_class = '';
}


//Retrieve fields from options page
$logo_light		=	get_field('logo_light', 'option');  //Logo for Light Background
$logo_dark		=	get_field('logo_dark', 'option');   //Logo for Dark Background

?>


<div class="row header">
	<div class="full-row">
		<div class="blocks-container-navbar">

			<div class="logo">
				<a href="<?php echo home_url(); ?>">
					<img src="<?=$logo_dark['url']?>" alt="<?=$logo_dark['alt']?>">
				</a>
			</div>

				<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
				<?php min_responsive_menu_button(); ?>

				
		</div>
	</div>
</div>